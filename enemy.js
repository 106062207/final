function Sawmove(saws){
  var speed = 1;
  saws.forEachAlive(function(child){
    // console.log(child.x+','+child.y);
    child.rotation+=3;
    if(child.TWO){
      //child move between position and patrol
      if(child.positiony==child.patroly){
        if(child.x==child.positionx){
          child.state = 1;
        }
        else if(child.state==1&&((child.positionx<child.patrolx&&child.x>child.patrolx)||(child.positionx>child.patrolx&&child.x<child.patrolx))){
          child.state = 2;
        }
        else if(child.x==child.patrolx){
          child.state = 3;
        }
        else if(child.state==3&&((child.positionx<child.patrolx&&child.x<child.positionx)||(child.positionx>child.patrolx&&child.x>child.positionx))){
          child.state = 4;
        }
      
        if(child.state==1){
          if(child.patrolx>child.positionx){
            child.reset(child.x+speed,child.y);
          }
          else if(child.patrolx<child.positionx){
            child.reset(child.x-speed,child.y);
          }
        }
        else if(child.state==2){
            child.reset(child.patrolx,child.patroly);
        }
        else if(child.state==3){
          if(child.patrolx>child.positionx){
            child.reset(child.x-speed,child.y);
          }
          else if(child.patrolx<child.positionx){
            child.reset(child.x+speed,child.y);
          }
        }
        else if(child.state==4){
          child.reset(child.positionx,child.positiony);
        }
      }
      if(child.positionx==child.patrolx){
        if(child.y==child.positiony){
          child.state = 1;
        }
        else if(child.state==1&&((child.positiony<child.patroly&&child.y>child.patroly)||(child.positiony>child.patroly&&child.y<child.patroly))){
          child.state = 2;
        }
        else if(child.y==child.patroly){
          child.state = 3;
        }
        else if(child.state==3&&((child.positiony<child.patroly&&child.y<child.positiony)||(child.positiony>child.patroly&&child.y>child.positiony))){
          child.state = 4;
        }
      
        if(child.state==1){
          if(child.patroly>child.positiony){
              child.reset(child.x,child.y+speed);
          }
          else if(child.patroly<child.positiony){
              child.reset(child.x,child.y-speed);
          }
        }
        else if(child.state==2){
            child.reset(child.patrolx,child.patroly);
        }
        else if(child.state==3){
          if(child.patroly>child.positiony){
              child.reset(child.x,child.y-speed);
          }
          else if(child.patroly<child.positiony){
              child.reset(child.x,child.y+speed);
          }
        }
        else if(child.state==4){
          child.reset(child.positionx,child.positiony);
        }
      }
        // console.log(child+' inpatrol between'+'{'+child.x+','+child.y+'} and {'+child.patrolx+','+child.patroly+'}');
    }
    else{
     // move between four points clockwise
      if(child.x==child.positionx&&child.y==child.positiony){
        child.state = 2;
      }
      else if(child.state==2&&((child.patrolx>child.positionx&&child.x>child.patrolx)||(child.patroly>child.positiony&&child.y>child.patroly)||(child.patrolx<child.positionx&&child.x<child.patrolx)||(child.patroly<child.positiony&&child.y<child.patroly))){
        child.state = 3;
      }
      else if(child.x==child.patrolx&&child.y==child.patroly){
        child.state = 4;
      }
      else if(child.state==4&&((child.patrolx1>child.patrolx&&child.x>child.patrolx1)||(child.patroly1>child.patroly&&child.y>child.patroly1)||(child.patrolx1<child.patrolx&&child.x<child.patrolx1)||(child.patroly1<child.patroly&&child.y<child.patroly1))){
        child.state = 5;
      }
      else if(child.x==child.patrolx1&&child.y==child.patroly1){
        child.state = 6;
      }
      else if(child.state==6&&((child.patrolx2>child.patrolx1&&child.x>child.patrolx2)||(child.patroly2>child.patroly1&&child.y>child.patroly2)||(child.patrolx2<child.patrolx1&&child.x<child.patrolx2)||(child.patroly2<child.patroly1&&child.y<child.patroly2))){
        child.state = 7;
      }
      else if(child.x==child.patrolx2&&child.y==child.patroly2){
        child.state = 8;
      }
      else if(child.state==8&&((child.positionx>child.patrolx2&&child.x>child.positionx)||(child.positiony>child.patroly2&&child.y>child.positiony)||(child.positionx<child.patrolx2&&child.x<child.positionx)||(child.positiony<child.patroly2&&child.y<child.positiony))){
        child.state = 1;
      }


      if(child.state == 1){
        child.reset(child.positionx,child.positiony);
      }
      else if(child.state==2){
        if(child.positionx<child.patrolx&&child.positiony==child.patroly){
            child.reset(child.x+speed,child.y);
        }
        else if(child.positionx>child.patrolx&&child.positiony==child.patroly){
            child.reset(child.x-speed,child.y);
        }
        else if(child.positionx==child.patrolx&&child.positiony<child.patroly){
            child.reset(child.x,child.y+speed);
        }
        else if(child.positionx==child.patrolx&&child.positiony>child.patroly){
            child.reset(child.x,child.y-speed);
        }
      }
      else if(child.state==3){
        child.reset(child.patrolx,child.patroly);
      }
      else if(child.state==4){
        if(child.patrolx<child.patrolx1&&child.patroly==child.patroly1){
            child.reset(child.x+speed,child.y);
        }
        else if(child.patrolx>child.patrolx1&&child.patroly==child.patroly1){
            child.reset(child.x-speed,child.y);
        }
        else if(child.patrolx==child.patrolx1&&child.patroly<child.patroly1){
            child.reset(child.x,child.y+speed);
        }
        else if(child.patrolx==child.patrolx1&&child.patroly>child.patroly1){
            child.reset(child.x,child.y-speed);
        }
      }
      else if(child.state==5){
        child.reset(child.patrolx1,child.patroly1);
      }
      else if(child.state==6){
        if(child.patrolx1<child.patrolx2&&child.patroly1==child.patroly2){
            child.reset(child.x+speed,child.y);
        }
        else if(child.patrolx1>child.patrolx2&&child.patroly1==child.patroly2){
            child.reset(child.x-speed,child.y);
        }
        else if(child.patrolx1==child.patrolx2&&child.patroly1<child.patroly2){
            child.reset(child.x,child.y+speed);
        }
        else if(child.patrolx1==child.patrolx2&&child.patroly1>child.patroly2){
            child.reset(child.x,child.y-speed);
        }
      }
      else if(child.state==7){
        child.reset(child.patrolx2,child.patroly2);
      }
      else if(child.state==8){
        if(child.patrolx2<child.positionx&&child.patroly2==child.positiony){
            child.reset(child.x+speed,child.y);
        }
        else if(child.patrolx2>child.positionx&&child.patroly2==child.positiony){
            child.reset(child.x-speed,child.y);
        }
        else if(child.patrolx2==child.positionx&&child.patroly2<child.positiony){
            child.reset(child.x,child.y+speed);
        }
        else if(child.patrolx2==child.positionx&&child.patroly2>child.positiony){
            child.reset(child.x,child.y-speed);
        }
      }
    }
  });
}

function DrawMap2saws(saws){
  var saw1 = saws.getFirstDead();
  if (!saw1) { return;}
  saw1.reset(30,270);
  saw1.patrolx = 110;
  saw1.patroly = 270;
  saw1.TWO = true;
  
  var saw2 = saws.getFirstDead();
  if (!saw2) { return;}
  saw2.reset(80,340);
  saw2.patrolx = 80;
  saw2.patroly = 430;
  saw2.TWO = true;
        
  var saw3 = saws.getFirstDead();
  if (!saw3) { return;}
  saw3.reset(115,490);
  saw3.patrolx = 115;
  saw3.patroly = 630;
  saw3.TWO = true;
  
  var saw4 = saws.getFirstDead();
  if (!saw4) { return;}
  saw4.reset(170,250);
  saw4.patrolx = 240;
  saw4.patroly = 250;
  saw4.patrolx1 = 240;
  saw4.patroly1 = 330;
  saw4.patrolx2 = 170;
  saw4.patroly2 = 330;
  saw4.TWO = false;
  
  var saw5 = saws.getFirstDead();
  if (!saw5) { return;}
  saw5.reset(170,410);
  saw5.patrolx = 240;
  saw5.patroly = 410;
  saw5.patrolx1 = 240;
  saw5.patroly1 = 490;
  saw5.patrolx2 = 170;
  saw5.patroly2 = 490;
  saw5.TWO = false;
  
  var saw6 = saws.getFirstDead();
  if (!saw6) { return;}
  saw6.reset(170,570);
  saw6.patrolx = 170;
  saw6.patroly = 660;
  saw6.patrolx1 = 240;
  saw6.patroly1 = 660;
  saw6.patrolx2 = 240;
  saw6.patroly2 = 570;
  saw6.TWO = false;
  
  var saw7 = saws.getFirstDead();
  if (!saw7) { return;}
  saw7.reset(300,250);
  saw7.patrolx = 300;
  saw7.patroly = 330;
  saw7.patrolx1 = 360;
  saw7.patroly1 = 330;
  saw7.patrolx2 = 360;
  saw7.patroly2 = 250;
  saw7.TWO = false;
  
  var saw8 = saws.getFirstDead();
  if (!saw8) { return;}
  saw8.reset(360,490);
  saw8.patrolx = 300;
  saw8.patroly = 490;
  saw8.patrolx1 = 300;
  saw8.patroly1 = 410;
  saw8.patrolx2 = 360;
  saw8.patroly2 = 410;
  saw8.TWO = false;

  
  var saw9 = saws.getFirstDead();
  if (!saw9) { return;}
  saw9.reset(300,660);
  saw9.patrolx = 360;
  saw9.patroly = 660;
  saw9.patrolx1 = 360;
  saw9.patroly1 = 570;
  saw9.patrolx2 = 300;
  saw9.patroly2 = 570;
  saw9.TWO = false;
  
  var saw10 = saws.getFirstDead();
  if (!saw10) { return;}
  saw10.reset(340,710);
  saw10.patrolx = 470;
  saw10.patroly = 710;
  saw10.TWO = true;
  
  var saw11 = saws.getFirstDead();
  if (!saw11) { return;}
  saw11.reset(420,280);
  saw11.patrolx = 420;
  saw11.patroly = 430;
  saw11.TWO = true;
  
  var saw12 = saws.getFirstDead();
  if (!saw12) { return;}
  saw12.reset(490,140);
  saw12.patrolx = 490;
  saw12.patroly = 630;
  saw12.TWO = true;
  
  var saw13 = saws.getFirstDead();
  if (!saw13) { return;}
  saw13.reset(570,210);
  saw13.patrolx = 570;
  saw13.patroly = 650;
  saw13.TWO = true;
  
  var saw14 = saws.getFirstDead();
  if (!saw14) { return;}
  saw14.reset(640,650);
  saw14.patrolx = 640;
  saw14.patroly = 210;
  saw14.TWO = true;
  
  var saw15 = saws.getFirstDead();
  if (!saw15) { return;}
  saw15.reset(710,330);
  saw15.patrolx = 710;
  saw15.patroly = 660;
  saw15.TWO = true;
  
  var saw16 = saws.getFirstDead();
  if (!saw16) { return;}
  saw16.reset(840,280);
  saw16.patrolx = 910;
  saw16.patroly = 280;
  saw16.patrolx1 = 910;
  saw16.patroly1 = 360;
  saw16.patrolx2 = 840;
  saw16.patroly2 = 360;
  saw16.TWO = false;
  
  var saw17 = saws.getFirstDead();
  if (!saw17) { return;}
  saw17.reset(910,450);
  saw17.patrolx = 910;
  saw17.patroly = 610;
  saw17.TWO = true;
}

function DrawMap4saws(saws){
  var saw1 = saws.getFirstDead();
  if (!saw1) { return;}
  saw1.reset(60,70);
  saw1.patrolx = 840;
  saw1.patroly = 70;
  saw1.patrolx1 = 840;
  saw1.patroly1 = 150;
  saw1.patrolx2 = 60;
  saw1.patroly2 = 150;
  saw1.TWO = false;
  
  var saw2 = saws.getFirstDead();
  if (!saw2) { return;}
  saw2.reset(840,70);
  saw2.patrolx = 840;
  saw2.patroly = 150;
  saw2.patrolx1 = 60;
  saw2.patroly1 = 150;
  saw2.patrolx2 = 60;
  saw2.patroly2 = 70;
  saw2.TWO = false;
        
  var saw3 = saws.getFirstDead();
  if (!saw3) { return;}
  saw3.reset(60,150);
  saw3.patrolx = 840;
  saw3.patroly = 150;
  saw3.patrolx1 = 840;
  saw3.patroly1 = 70;
  saw3.patrolx2 = 60;
  saw3.patroly2 = 70;
  saw3.TWO = false;
  
  var saw4 = saws.getFirstDead();
  if (!saw4) { return;}
  saw4.reset(60,300);
  saw4.patrolx = 850;
  saw4.patroly = 300;
  saw4.patrolx1 = 850;
  saw4.patroly1 = 360;
  saw4.patrolx2 = 60;
  saw4.patroly2 = 360;
  saw4.TWO = false;
  
  var saw5 = saws.getFirstDead();
  if (!saw5) { return;}
  saw5.reset(850,360);
  saw5.patrolx = 850;
  saw5.patroly = 300;
  saw5.patrolx1 = 60;
  saw5.patroly1 = 300;
  saw5.patrolx2 = 60;
  saw5.patroly2 = 360;
  saw5.TWO = false;
  
  var saw6 = saws.getFirstDead();
  if (!saw6) { return;}
  saw6.reset(60,360);
  saw6.patrolx = 850;
  saw6.patroly = 360;
  saw6.patrolx1 = 850;
  saw6.patroly1 = 300;
  saw6.patrolx2 = 60;
  saw6.patroly2 = 300;
  saw6.TWO = false;
  
  var saw7 = saws.getFirstDead();
  if (!saw7) { return;}
  saw7.reset(90,480);
  saw7.patrolx = 900;
  saw7.patroly = 480;
  saw7.TWO = true;
  
  var saw8 = saws.getFirstDead();
  if (!saw8) { return;}
  saw8.reset(900,540);
  saw8.patrolx = 30;
  saw8.patroly = 540;
  saw8.TWO = true;

  
  var saw9 = saws.getFirstDead();
  if (!saw9) { return;}
  saw9.reset(890,750);
  saw9.patrolx = 30;
  saw9.patroly = 750;
  saw9.TWO = true;
  
}