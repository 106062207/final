var game = new Phaser.Game(950, 800, Phaser.CANVAS);

game.global = {
  score: 0,
  level: 1,
  titleBGM: null,
  gameBGM: null,
  musicvolume: 1,
  soundvolume: 1
};
 
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('set',settingState);
game.state.add('select', selectState);
game.state.add('tutorial', tutorialState);
game.state.add('play', playState);
game.state.start('boot');