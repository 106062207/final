var loadState = {
  preload: function() {
    game.stage.backgroundColor = '#000000';

    var loadingLabel = game.add.text(game.width/2, game.height*0.4, 'loading...', {font:'30px Arial', fill:'#ffffff'});
    loadingLabel.anchor.setTo(0.5, 0.5);
    var progressBar = game.add.sprite(game.width/2, game.height*0.6, 'progressBar');
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar);

    console.log("loading assets...");
    //setting object
    game.load.image('bar','assets/sliderBar.png');
    game.load.image('whiteCircle','assets/sliderCircle.png');

    // Walls
    game.load.image('steelBlock', "assets/steelBlock.png");
    game.load.image('steelBone', "assets/steelBone.png");
    game.load.image('steelSlope', "assets/steelSlope.png");
    // TileSet
    game.load.image('tileset', 'assets/tileset.png');
    // TileMap
    game.load.tilemap('tilemap01', 'assets/map1.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('tilemap02', 'assets/map2.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('tilemap03', 'assets/map3.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('tilemap04', 'assets/map4.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('tilemap05', 'assets/map5.json', null, Phaser.Tilemap.TILED_JSON);
     // sound
     game.load.audio('die', 'assets/die.mp3');
     game.load.audio('titleBGM', 'assets/titleBGM.mp3');
     game.load.audio('gameBGM', 'assets/gameBGM.mp3');
     game.load.audio('gunSound', 'assets/gun.mp3');
     game.load.audio('shotgunSound', 'assets/shotgun.mp3');
     game.load.audio('pretextSound', 'assets/pretext.mp3');
     game.load.audio('shieldSound', 'assets/shield.wav');
     game.load.audio('swordSound', 'assets/sword.wav');
     game.load.audio('clearSound', 'assets/clear.mp3');
    // GameObjects
    game.load.image('title', "assets/title.png");
    game.load.image('menu', "assets/menu.png");
    game.load.image('cursorRight', "assets/cursorRight.png");
    game.load.image('cursorDown', "assets/cursorDown.png");
    game.load.image('rain', "assets/rain.png");
    game.load.image('coin', 'assets/coin.png');
    game.load.image('mine', 'assets/mine.png');
    game.load.image('saw', 'assets/saw.png');
    game.load.image('circle', 'assets/circle.png');
    game.load.image('missle', 'assets/missle.png');
    game.load.image('sword', 'assets/sword.png');
    game.load.image('sworditem', 'assets/sworditem.png');
    game.load.image('pretext', 'assets/pretext.png');
    game.load.image('shield', 'assets/shield.png');
    game.load.image('shell', 'assets/bullet.png');
    game.load.image('gun', 'assets/gun.png');
    game.load.image('shotgun', 'assets/shotgun.png');
    game.load.image('portal', 'assets/portal.png');
    game.load.image('key', 'assets/key.png');
    // Gate
    game.load.spritesheet('gate', 'assets/gate.png', 20, 30, 5);
    // Player
    game.load.spritesheet('playerSheet', "assets/playerFull.png", 22, 30, 34);
    //select object
    game.load.image('select1', 'assets/select1.png');
    game.load.image('select2', 'assets/select2.png');
    game.load.image('select3', 'assets/select3.png');
    game.load.image('select4', 'assets/select4.png');
    game.load.image('select5', 'assets/select5.png');
    console.log('load complete');
  },
  create: function() {
    game.state.start('menu');
  }
}
