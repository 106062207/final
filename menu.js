var menuState = {
  preload: function() {
    game.stage.backgroundColor = '#000000';
    this.inputTexts = setInputTexts();
  },
  create: function() {
    var cntX = game.width/2;
    var cntY = game.height/2;
    var font = {font:'bold 25px Arial', fill:'#ffffff'};

    var background = game.add.sprite(cntX, cntY - 100, 'menu');
    background.anchor.setTo(0.5, 0.5);
    var title = game.add.sprite(cntX - 250, cntY*2 - 120, 'title');
    title.anchor.setTo(0.5, 0.5);

    var startText = setText(cntX, cntY*2 - 100, 'start', font, this.inputTexts);
    var setsText = setText(cntX + 150, cntY*2 - 100, 'settings', font, this.inputTexts);
    var helpText = setText(cntX + 300, cntY*2 - 100, 'tutorial', font, this.inputTexts);
    var scoreText = setText(cntX, cntY+100, 'score:' + game.global.score, font, null);

    this.rains = initGroup();
    this.rains.createMultiple(120, 'rain');
    this.rains.setAll('body.gravityScale', 0.6);

    startText.events.onInputDown.add(this.start, this);
    setsText.events.onInputDown.add(this.setting, this);
    helpText.events.onInputDown.add(this.help, this);

    //---sound---//
    if(!game.global.titleBGM){
      game.global.titleBGM = game.add.audio('titleBGM');
      game.global.titleBGM.volume = game.global.musicvolume;
    }
    else{
      game.global.titleBGM.volume = game.global.musicvolume;
      if(!game.global.titleBGM.isPlaying){
        game.global.titleBGM.play();
        game.global.titleBGM.loop = true;
      }
    }
  },

  update: function() {
    if(!game.global.titleBGM.isPlaying){
      game.global.titleBGM.play();
    }
    raining(this.rains, 30, 920, 50, 550);
  },

  start: function() {
    console.log("start game...");
    game.state.start('select');
  },
  setting: function() {
    console.log("To setting...");
    game.state.start('set');
  },
  help: function() {
    console.log("To tutorial...");
    game.state.start('tutorial');
  }
}
