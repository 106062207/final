function missleLaunch(missleCount, missles, player, ori){
  if(missleCount == 300){
    missleCount = 0;
    var missle = missles.getFirstDead();
    if(!missle){return;}
    missle.anchor.setTo(0.5, 0.5);
    missle.scale.setTo(0.25, 0.25);
    missle.reset(ori.x, ori.y);
    var dirX = (player.body.x - missle.body.x);
    var dirY = (player.body.y - missle.body.y);
    var len = Math.sqrt(dirY*dirY + dirX*dirX);
    missle.angle = (dirY < 0)? 360 - Math.acos(dirX/len) * 180 / Math.PI:Math.acos(dirX/len) * 180 / Math.PI;
    //console.log(this.missleVector[this.missles.getIndex(missle)].bx, this.missleVector[this.missles.getIndex(missle)].by, this.missleVector[this.missles.getIndex(missle)].ax, this.missleVector[this.missles.getIndex(missle)].ay);
    //console.log(missle.angle);
  }
  else{
    missleCount++;
  }

  return missleCount;
}

function missleMove(missles, player, tiles){
  var playerX = player.body.x;
  var playerY = player.body.y;

  missles.forEachAlive(function(child){
    for (var i = 0; i < tiles.length; i++) {
      //game.physics.ninja.collide(this.missles, this.tiles[i].tile, function(missle, tile){console.log('collide');missle.kill();}, null, this);
      if(Math.abs(tiles[i].tile.x - child.x) <= 10 && Math.abs(tiles[i].tile.y - child.y) <= 12){
        child.kill();
      }
    }
  });

  missles.forEachAlive(function(child){
    if(player.alive){
      var dirX = (playerX - child.body.x);
      var dirY = (playerY - child.body.y);
      var len = Math.sqrt(dirY*dirY + dirX*dirX);
      var turnAng = 1.6;
      var newAng = (dirY < 0)? 360 - Math.acos(dirX/len) * 180 / Math.PI : Math.acos(dirX/len) * 180 / Math.PI;
      var vecX = Math.cos(child.angle * Math.PI / 180);
      var vecY = Math.sin(child.angle * Math.PI / 180);
      //console.log(Math.cos(angle));
      if(Math.abs(newAng-child.angle) < turnAng){
        child.angle = newAng;
        //console.log('reg');
      }
      else{
        var cross = (vecX)*(playerY - child.body.y) - (vecY)*(playerX - child.body.x);
        if(cross < 0){
          //console.log('left');
          child.angle -= turnAng;
        }
        else{
          //console.log('right');
          child.angle += turnAng;
        }
      }

      //console.log(child.angle);
      //console.log(Math.cos(child.angle * Math.PI / 180), Math.sin(child.angle * Math.PI / 180));
      child.reset(child.body.x + 3 * Math.cos(child.angle * Math.PI / 180), child.body.y + 3 * Math.sin(child.angle * Math.PI / 180));
      
      //console.log(child.angle);

      if(Math.abs(playerX - child.body.x) <= 25 && Math.abs(playerY - child.body.y) <= 24){
        child.kill();
        playState.playerDie();
      }
    }
    else{
      child.reset(child.body.x, child.body.y - 5);
    }
  });
}

function bulletLaunch(missleCount, missles, ori, index){
  if(missleCount == 100){
    missleCount = 0;
    var missle = missles.getFirstDead();
    if(!missle){return;}
    missle.anchor.setTo(0.5, 0.5);
    missle.scale.setTo(0.25, 0.25);
    missle.reset(ori.x, ori.y);

    if(index == 0){
      missle.angle = 180 + Math.acos(92/Math.sqrt(8633)) * 180 / Math.PI;
    }
    else if(index == 1){
      missle.angle = Math.acos(90/Math.sqrt(8356)) * 180 / Math.PI;
    }
    else if(index == 2){
      missle.angle = 270;
    }
    else if(index == 3){
      missle.angle = (Math.acos(-84/Math.sqrt(7225)) * 180 / Math.PI);
    }
    else if(index == 4){
      missle.angle = 0;
    }
  }
  else{
    missleCount++;
  }

  return missleCount;
}

function bulletMove(missles, index, tiles, player){
  missles.forEachAlive(function(child){
    if(index == 0){
      child.reset(child.x - 4 * 92/Math.sqrt(8633), child.y - 4 * 13/Math.sqrt(8633));
    }
    else if(index == 1){
      child.reset(child.x + 4 * 90/Math.sqrt(8356), child.y + 4 * 16/Math.sqrt(8325));
    }
    else if(index == 2){
      child.reset(child.x, child.y - 4);
    }
    else if(index == 3){
      child.reset(child.x - 4 * 84/Math.sqrt(7200), child.y + 4 * 13/Math.sqrt(7225));
    }
    else if(index == 4){
      child.reset(child.x + 4, child.y);
    }

    if(Math.abs(player.body.x - child.body.x) <= 15 && Math.abs(player.body.y - child.body.y) <= 15){
      child.kill();
      playState.playerDie();
    }

    for (var i = 0; i < tiles.length; i++) {
      //game.physics.ninja.collide(this.missles, this.tiles[i].tile, function(missle, tile){console.log('collide');missle.kill();}, null, this);
      if(Math.abs(tiles[i].tile.x - child.body.x) <= 10 && Math.abs(tiles[i].tile.y - child.body.y) <= 12){
        child.kill();
      }
    }
  });
}