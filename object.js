// Add Functions
function initGroup() {
  var group = game.add.group();

  group.physicsBodyType = Phaser.Physics.NINJA;
  group.enableBody = true;

  return group;
}

function addSprite(posX, posY, sprite, ratio, group, scaleX, scaleY) {
  var object = game.add.sprite(posX, posY, sprite);

  if (scaleX == undefined) scaleX = 1;
  if (scaleY == undefined) scaleY = 1;

  object.anchor.setTo(0.5, 0.5);
  object.scale.setTo(ratio*scaleX, ratio*scaleY);
  if (group != null) group.add(object);

  return object;
}

function addTile(posX, posY, id, sprite, ratio, group, scaleX, scaleY) {
  var tile = game.add.sprite(posX, posY, sprite);

  if (scaleX == undefined) scaleX = 1;
  if (scaleY == undefined) scaleY = 1;
  tile.anchor.setTo(0.5, 0.5);
  tile.scale.setTo(ratio*scaleX, ratio*scaleY);

  game.physics.ninja.enableTile(tile, id);
  if (group != null) group.add(tile);
}

function addWeapon(posX, posY, key, group) {
  var object = game.add.sprite(posX, posY, key);
  object.anchor.setTo(0.5, 0.5);
  switch(key) {
    case 'sword':
      object.scale.setTo(0.2, 0.2);
      break;
    case 'shotgun':
      object.scale.setTo(0.3, 0.3);
      break;
  }
  game.physics.ninja.enableAABB(object);
  object.body.gravityScale = 0;

  if (group != null) group.add(object);
  return object;
}

function addTrap(posX, posY, key, group, Vts, Vxs, Vys) {
  var trap = game.add.sprite(posX, posY, key);
  game.physics.ninja.enableAABB(trap);
  trap.body.immovable = true;
  trap.body.gravityScale = 0;
  // Added movement data
  trap.state = 0;
  trap.count = 0;
  if (Vts == undefined) trap.Vts = [0];
  else trap.Vts = Vts;
  if (Vxs == undefined) trap.Vxs = [0];
  else trap.Vxs = Vxs;
  if (Vys == undefined) trap.Vys = [0];
  else trap.Vys = Vys;

  if (group != null) group.add(trap);
  return trap;
}

// Config Functions
function setWallsPhysics(group) {
  group.setAll('body.immovable', true);
  group.setAll('body.gravityScale', 0);
}

function isFinish(groupA, groupB, groupC) {
  if (groupA != undefined) {
    if (groupA.total > 0) return false;
  }
  if (groupB != undefined) {
    if (groupB.total > 0) return false;
  }
  if (groupC != undefined) {
    if (groupC.total > 0) return false;
  }
  return true;
}

// Event Functions
function getDrop(player, tool) {
  player.inHand = game.add.sprite(player.x, player.y, tool.key);
  var inHand = player.inHand;

  inHand.anchor.setTo(0.5, 0.5);
  switch(tool.key) {
    case 'sword':
      inHand.scale.setTo(0.1, 0.1);
      player.ammos.add(inHand);
      break;
    case 'shotgun':
      inHand.scale.setTo(0.12, 0.12);
      player.ammos.createMultiple(25, 'shell');
      break;
  }
  game.physics.ninja.enableAABB(inHand);
  inHand.body.gravityScale = 0;
  inHand.dur = 5;

  tool.kill();
}

function hitPlayer(player, trap) {
  player.kill();
  if (player.inHand != null) player.inHand.kill();
  trap.kill();
  game.time.events.add(500, function() {game.state.start('menu');}, this);
}

function moveTraps(traps) {
  traps.forEachAlive(function(trap) {
    var modes = trap.Vts.length;
    if (modes != 1) {
      if (trap.count == trap.Vts[trap.state]) {
        trap.state = (trap.state + 1)%modes;
        trap.count = 0;
      } else {
        trap.count++;
      }
      var posX = trap.x;
      var posY = trap.y;
      trap.reset(posX + trap.Vxs[trap.state], posY + trap.Vys[trap.state]);
      if (trap.name == 'saw') trap.rotation += 1;
    }
  }, this);
}

function moveBullets(bullets) {
  bullets.forEachAlive(function(bullet) {
    switch(bullet.key) {
      case 'shell':
        var posX = bullet.x + bullet.Vx;
        var posY = bullet.y + bullet.Vy;
        bullet.reset(posX, posY);
        break;
    }
  }, this);
}

function hitTrap(weapons, traps) {
  weapons.forEachAlive(function(weapon, traps){
    traps.forEachAlive(function(trap, weapon){
      var inX = 0;
      var inY = 0;
      var bonus = 1;
      if (weapon.angle != 0) bonus = 2;
      switch(trap.key) {
        case 'mine':
          inX = (Math.abs(weapon.x - trap.x) < 20*bonus);
          inY = (Math.abs(weapon.y - trap.y) < 20*bonus);
          break;
        case 'saw':
          inX = (Math.abs(weapon.x - trap.x) < 25*bonus);
          inY = (Math.abs(weapon.y - trap.y) < 25*bonus);
          break;
      }
      if (inX && inY) {
        switch(weapon.key) {
          case 'sword':
            weapon.dur--;
            break;
          case 'shell':
            weapon.kill();
            weapons.removeChild(weapon);
            break;
        }
        trap.kill();
        traps.removeChild(trap);
      }
    }, this, weapon);
  }, this, traps);
}

function holding(player) {
  if (player.inHand != null) {
    var inHand = player.inHand;
    var scaleX = player.scale.x;
    var dist;
    switch(player.inHand.key) {
      case 'sword':
        dist = 10;
        break;
      case 'shotgun':
        dist = 20;
        break;
    }
    if (inHand.scale.x*player.scale.x > 0) inHand.scale.x *= -1;
    inHand.x = player.x + dist*((scaleX > 0) - (scaleX < 0));
    inHand.y = player.y;
    inHand.body.x = player.body.x + dist*((scaleX > 0) - (scaleX < 0));
    inHand.body.y = player.body.y;
  }
}

function wearOut(player) {
  if (player.inHand != null) {
    if (player.inHand.dur <= 0) {
      player.inHand.destroy();
      player.inHand = null;
    }
  }
}

// Effects
function raining(rains, minX, maxX, minY, maxY) {
  // New drops
  for (var i = 0; i < 2; i++) {
    var drop = rains.getFirstDead();
    if (drop != null) {
      drop.reset(game.rnd.integerInRange(minX, maxX), minY);
      drop.body.moveDown(200);
    }
  }
  // Remove drops
  rains.forEachAlive(function(drop) {
    if (drop.y >= maxY) drop.kill();
    else if (drop.y > maxY - 150) {
      if (game.rnd.integerInRange(1, 10) > 8) drop.kill();
    }
  }, this);
}
