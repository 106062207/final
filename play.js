var playState = {
  create: function(){
    this.game.stage.backgroundColor = "#ffffff";

    switch(game.global.level){
      case 1:
        this.map = game.add.tilemap('tilemap01');
        break;
      case 2:
        this.map = game.add.tilemap('tilemap02');
        break;
      case 3:
        this.map = game.add.tilemap('tilemap03');
        break;
      case 4:
        this.map = game.add.tilemap('tilemap04');
        break;
      case 5:
        this.map = game.add.tilemap('tilemap05');
        break;
    }

    this.map.addTilesetImage('tileset');
    this.Layer1 = this.map.createLayer('Layer1');
    this.Layer1.resizeWorld();

    this.game.physics.startSystem(Phaser.Physics.NINJA);
    game.physics.ninja.gravity = 0.2;

    this.slopeMap = {'1': 1, '2': 2, '3': 5, '4': 4, '5': 3, '6': 0, '7': 0, '8': 0};
    this.tiles = game.physics.ninja.convertTilemap(this.map, this.Layer1, this.slopeMap);

    this.coins = game.add.group();
    this.coins.physicsBodyType = Phaser.Physics.NINJA;
    this.coins.enableBody = true;
    this.map.createFromObjects('Object Layer 1', 8, 'coin', 0, true, false, this.coins);
    this.coins.setAll('body.gravityScale', 0);
    this.coins.setAll('body.immovable', true);
    
    switch(game.global.level){
      case 1:
        this.mines = game.add.group();
        this.mines.physicsBodyType = Phaser.Physics.NINJA;
        this.mines.enableBody = true;
        this.map.createFromObjects('Object Layer 2', 7, 'mine', 0, true, false, this.mines);
        this.mines.setAll('body.gravityScale', 0);
        this.mines.setAll('body.immovable', true);

        this.shotgun = game.add.sprite(700,390, 'shotgun');
        this.shotgun.anchor.setTo(0.5, 0.5);
        this.shotgun.scale.setTo(0.18, 0.18);
        game.physics.ninja.enableAABB(this.shotgun);
        this.shotgun.body.gravityScale = 0;

        this.proparr = game.add.group();
        this.proparr.addChild(this.shotgun);
  
        this.key = game.add.sprite(180, 390, 'key');
        this.getKey = false;
        this.gate = game.add.sprite(130, 390, 'gate');
        this.gate.animations.add('open', [1, 2, 3, 4], 10, false);
  
        this.player = setupPlayer(130, 570, 'playerSheet');
        break;
      case 2:
        this.saws = game.add.group();
        this.saws.physicsBodyType = Phaser.Physics.NINJA;
        this.saws.enableBody = true;
        this.saws.createMultiple(20,'saw');
        this.saws.setAll('body.gravityScale', 0);
        DrawMap2saws(this.saws);
        this.saws.forEachAlive(function(child){
          child.positionx = child.x;
          child.positiony = child.y;
        });

        this.sword = game.add.sprite(20,200, 'sworditem');
        this.sword.anchor.setTo(0.5, 0.5);
        this.sword.scale.setTo(0.15, 0.15);
        game.physics.ninja.enableAABB(this.sword);
        this.sword.body.gravityScale = 0;

        this.gun = game.add.sprite(350,730, 'gun');
        this.gun.anchor.setTo(0.5, 0.5);
        this.gun.scale.setTo(1.8, 1.8);
        game.physics.ninja.enableAABB(this.gun);
        this.gun.body.gravityScale = 0;

        this.proparr = game.add.group();
        this.proparr.addChild(this.sword);
        this.proparr.addChild(this.gun);
  
        this.key = game.add.sprite(60, 590, 'key');
        this.getKey = false;
        this.gate = game.add.sprite(890, 140, 'gate');
        this.gate.animations.add('open', [1, 2, 3, 4], 10, false);
  
        this.player = setupPlayer(10, 690, 'playerSheet');
        break;
      case 3:
        this.mines = game.add.group();
        this.mines.physicsBodyType = Phaser.Physics.NINJA;
        this.mines.enableBody = true;
        this.map.createFromObjects('Object Layer 2', 7, 'mine', 0, true, false, this.mines);
        this.mines.setAll('body.gravityScale', 0);
        this.mines.setAll('body.immovable', true);
  
        this.portals = game.add.group();
        this.portals.physicsBodyType = Phaser.Physics.NINJA;
        this.portals.enableBody = true;
        this.map.createFromObjects('Object Layer 3', 6, 'portal', 0, true, false, this.portals);
        this.portals.setAll('body.gravityScale', 0);
        this.portals.setAll('body.immovable', true);
        this.portalEnter = 0;
  
        this.missleLauncher = game.add.sprite(580, 380, 'circle');
        this.missleLauncher.scale.setTo(0.2, 0.2);
  
        this.missles = game.add.group();
        this.missles.physicsBodyType = Phaser.Physics.NINJA;
        this.missles.enableBody = true;
        this.missles.createMultiple(10, 'missle');
        this.missleCount = 290;
        this.missles.setAll('body.gravityScale', 0);
        this.missles.setAll('body.bounce', 0);

        this.pretext = game.add.sprite(490,760, 'pretext');
        this.pretext.anchor.setTo(0.5, 0.5);
        this.pretext.scale.setTo(0.03, 0.03);
        game.physics.ninja.enableAABB(this.pretext);
        this.pretext.body.gravityScale = 0;

        this.gun = game.add.sprite(880,50, 'gun');
        this.gun.anchor.setTo(0.5, 0.5);
        this.gun.scale.setTo(1.8, 1.8);
        game.physics.ninja.enableAABB(this.gun);
        this.gun.body.gravityScale = 0;

        this.proparr = game.add.group();
        this.proparr.addChild(this.pretext);
        this.proparr.addChild(this.gun);
  
        this.key = game.add.sprite(840, 170, 'key');
        this.getKey = false;
        this.gate = game.add.sprite(30, 590, 'gate');
        this.gate.animations.add('open', [1, 2, 3, 4], 10, false);
  
        this.player = setupPlayer(20, 760, 'playerSheet');
        break;
      case 4:
        this.mines = game.add.group();
        this.mines.physicsBodyType = Phaser.Physics.NINJA;
        this.mines.enableBody = true;
        this.map.createFromObjects('Object Layer 2', 7, 'mine', 0, true, false, this.mines);
        this.mines.setAll('body.gravityScale', 0);
        this.mines.setAll('body.immovable', true);
  
        this.saws = game.add.group();
        this.saws.physicsBodyType = Phaser.Physics.NINJA;
        this.saws.enableBody = true;
        this.saws.createMultiple(20,'saw');
        this.saws.setAll('body.gravityScale', 0);
        DrawMap4saws(this.saws);
        this.saws.forEachAlive(function(child){
          child.positionx = child.x;
          child.positiony = child.y;
        });

        this.sword = game.add.sprite(890,160, 'sworditem');
        this.sword.anchor.setTo(0.5, 0.5);
        this.sword.scale.setTo(0.15, 0.15);
        game.physics.ninja.enableAABB(this.sword);
        this.sword.body.gravityScale = 0;

        this.shotgun = game.add.sprite(310,480, 'shotgun');
        this.shotgun.anchor.setTo(0.5, 0.5);
        this.shotgun.scale.setTo(0.18, 0.18);
        game.physics.ninja.enableAABB(this.shotgun);
        this.shotgun.body.gravityScale = 0;

        this.proparr = game.add.group();
        this.proparr.addChild(this.sword);
        this.proparr.addChild(this.shotgun);
  
        this.key = game.add.sprite(60, 300, 'key');
        this.getKey = false;
        this.gate = game.add.sprite(30, 760, 'gate');
        this.gate.animations.add('open', [1, 2, 3, 4], 10, false);
  
        this.player = setupPlayer(920, 180, 'playerSheet');
        break;
      case 5:
        this.missleLauncher1 = game.add.sprite(900, 160, 'circle');
        this.missleLauncher1.scale.setTo(0.2, 0.2);
        this.missleLauncher2 = game.add.sprite(20, 160, 'circle');
        this.missleLauncher2.scale.setTo(0.2, 0.2);
        this.missleLauncher3 = game.add.sprite(40, 700, 'circle');
        this.missleLauncher3.scale.setTo(0.2, 0.2);
        this.missleLauncher4 = game.add.sprite(900, 660, 'circle');
        this.missleLauncher4.scale.setTo(0.2, 0.2);
        this.missleLauncher5 = game.add.sprite(150, 590, 'circle');
        this.missleLauncher5.scale.setTo(0.2, 0.2);
  
        this.missles1 = game.add.group();
        this.missles1.physicsBodyType = Phaser.Physics.NINJA;
        this.missles1.enableBody = true;
        this.missles1.createMultiple(10, 'missle');
        this.missles1.setAll('body.gravityScale', 0);
        this.missles1.setAll('body.bounce', 0);
        this.missles1.setAll('anchor.setTo', 0.5, 0.5);
        this.missles2 = game.add.group();
        this.missles2.physicsBodyType = Phaser.Physics.NINJA;
        this.missles2.enableBody = true;
        this.missles2.createMultiple(10, 'missle');
        this.missles2.setAll('body.gravityScale', 0);
        this.missles2.setAll('body.bounce', 0);
        this.missles2.setAll('anchor.setTo', 0.5, 0.5);
        this.missles3 = game.add.group();
        this.missles3.physicsBodyType = Phaser.Physics.NINJA;
        this.missles3.enableBody = true;
        this.missles3.createMultiple(10, 'missle');
        this.missles3.setAll('body.gravityScale', 0);
        this.missles3.setAll('body.bounce', 0);
        this.missles3.setAll('anchor.setTo', 0.5, 0.5);
        this.missles4 = game.add.group();
        this.missles4.physicsBodyType = Phaser.Physics.NINJA;
        this.missles4.enableBody = true;
        this.missles4.createMultiple(10, 'missle');
        this.missles4.setAll('body.gravityScale', 0);
        this.missles4.setAll('body.bounce', 0);
        this.missles4.setAll('anchor.setTo', 0.5, 0.5);
        this.missles5 = game.add.group();
        this.missles5.physicsBodyType = Phaser.Physics.NINJA;
        this.missles5.enableBody = true;
        this.missles5.createMultiple(10, 'missle');
        this.missles5.setAll('body.gravityScale', 0);
        this.missles5.setAll('body.bounce', 0);
        this.missles5.setAll('anchor.setTo', 0.5, 0.5);
  
        this.missleCount = [50, 50, 50, 50, 50];
        this.launchPos = [{x: 910, y: 170}, {x: 30, y: 170}, {x: 50, y: 710}, {x: 910, y: 670}, {x: 160, y: 600}];

        this.shield = game.add.sprite(890,300, 'shield');
        this.shield.anchor.setTo(0.5, 0.5);
        this.shield.scale.setTo(0.2, 0.2);
        game.physics.ninja.enableAABB(this.shield);
        this.shield.body.gravityScale = 0;

        this.proparr = game.add.group();
        this.proparr.addChild(this.shield);
  
        this.key = game.add.sprite(30, 430, 'key');
        this.getKey = false;
        this.gate = game.add.sprite(920, 500, 'gate');
        this.gate.animations.add('open', [1, 2, 3, 4], 10, false);
  
        this.player = setupPlayer(20, 60, 'playerSheet');
        this.player.scale.setTo(0.8, 0.8);
        break;
    }
 
    cursors = game.input.keyboard.createCursorKeys();

    //---prop global---//
    this.haveProp = '';
    this.prop = null;

    this.keyQ = game.input.keyboard.addKey(Phaser.Keyboard.Q);

    this.shells = game.add.group();
    this.shells.createMultiple(20, 'shell');
    this.shellsNumber = 0;
    this.QisDown = false;
    this.EisDown = false;

    //---sound---//
    if(!game.global.gameBGM){
      game.global.gameBGM = game.add.audio('gameBGM');
    }
    game.global.gameBGM.volume = game.global.musicvolume;
    this.dieSound = game.add.audio('die');
    this.gunSound = game.add.audio('gunSound');
    this.shotgunSound = game.add.audio('shotgunSound');
    this.pretextSound = game.add.audio('pretextSound');
    this.shieldSound = game.add.audio('shieldSound');
    this.swordSound = game.add.audio('swordSound');
    this.clearSound = game.add.audio('clearSound');

    this.dieSound.volume = game.global.soundvolume;
    this.gunSound.volume = game.global.soundvolume;
    this.shotgunSound.volume = game.global.soundvolume;
    this.pretextSound.volume = game.global.soundvolume;
    this.shieldSound.volume = game.global.soundvolume;
    this.swordSound.volume = game.global.soundvolume;
    this.clearSound.volume = game.global.soundvolume;

    //return to Menu
    this.keyEsc = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
    this.keyEsc.onDown.add(function(){
      game.global.gameBGM.stop();
      game.state.start('menu');
    }, this);

    this.scoreLabel = game.add.text(5, 0, 'score:' + game.global.score, { font: '24px Arial', fill: '#000' });

    this.invincible = false;
    this.invincibleLabel = game.add.text(780, 0, 'invincible:false', { font: '24px Arial', fill: '#000' });
    this.keyB = game.input.keyboard.addKey(Phaser.Keyboard.B);
    this.keyB.onDown.add(function(){
      if(!this.invincible){
        this.invincibleLabel.text = 'invincible:true';
        this.invincible = true;
      }
      else{
        this.invincibleLabel.text = 'invincible:false';
        this.invincible = false;
      }
    }, this);

    this.isClear = false;
  },
 
  update: function(){
    if(!game.global.gameBGM.isPlaying){
      game.global.gameBGM.play();
    }
    for (var i = 0; i < this.tiles.length; i++) {
      this.player.body.aabb.collideAABBVsTile(this.tiles[i].tile);
    }
    
    moveByCursors(this.player, cursors, this.tiles);

    this.takeCoin();

    switch(game.global.level){
      case 1:
        this.takeMine();
        this.detectShellCollide(this.mines);
        this.propShotgunFunction(this.mines);
        break;
      case 2:
        Sawmove(this.saws);
        this.takeSaw();
        this.detectShellCollide(this.saws);
        this.propSwordFunction(this.saws);
        this.propGunFunction(this.saws);
        break;
      case 3:
        this.takeMine();
        this.detectShellCollide(this.mines);
        this.propGunFunction(this.mines);
  
        this.portalEnter = portal(this.portals, this.player, this.portalEnter);
        this.missleCount = missleLaunch(this.missleCount, this.missles, this.player, {x: 590, y: 390});
        missleMove(this.missles, this.player, this.tiles);
        this.propPretextFunction(this.missles);
        break;
      case 4:
        this.takeMine();
        this.detectShellCollide(this.mines);
        this.propSwordFunction(this.mines);
        this.propShotgunFunction(this.mines);
  
        Sawmove(this.saws);
        this.takeSaw();
        this.detectShellCollide(this.saws);
        this.propSwordFunction(this.saws);
        this.propShotgunFunction(this.saws);
        break;
      case 5:
        this.missleCount[0] = bulletLaunch(this.missleCount[0], this.missles1, this.launchPos[0], 0);
        this.missleCount[1] = bulletLaunch(this.missleCount[1], this.missles2, this.launchPos[1], 1);
        this.missleCount[2] = bulletLaunch(this.missleCount[2], this.missles3, this.launchPos[2], 2);
        this.missleCount[3] = bulletLaunch(this.missleCount[3], this.missles4, this.launchPos[3], 3);
        this.missleCount[4] = bulletLaunch(this.missleCount[4], this.missles5, this.launchPos[4], 4);
        bulletMove(this.missles1, 0, this.tiles, this.player);
        bulletMove(this.missles2, 1, this.tiles, this.player);
        bulletMove(this.missles3, 2, this.tiles, this.player);
        bulletMove(this.missles4, 3, this.tiles, this.player);
        bulletMove(this.missles5, 4, this.tiles, this.player);
        this.propShieldFunction(this.missles1);
        this.propShieldFunction(this.missles2);
        this.propShieldFunction(this.missles3);
        this.propShieldFunction(this.missles4);
        this.propShieldFunction(this.missles5);
    }

    this.takeKey();
    this.clear();

    //---prop update---/

    this.changeEQ();

    this.controlShell();

    this.detectHaveProp();

    this.controlProp();
  },

  playerDie: function(){
    if(!this.invincible){
      this.dieSound.play();
      this.player.kill();
      
      game.time.events.add(700, function(){
        game.global.score = 0;
        game.state.start('play');
      }, this);
    }
    
  },

  takeCoin: function(){
    var playerX = this.player.body.x;
    var playerY = this.player.body.y;
    //game.physics.ninja.overlap(this.player, this.coins, this.takeCoin, null, this);
    this.coins.forEachAlive(function(child){
      if(Math.abs(playerX - child.body.x) <= 16 && Math.abs(playerY - child.body.y) <= 20){
        child.kill();
        game.global.score += 10;
        playState.scoreLabel.text = 'score:' + game.global.score;
      }
    });
  },

  takeMine: function(){
    var playerX = this.player.body.x;
    var playerY = this.player.body.y;
    //game.physics.ninja.overlap(this.player, this.coins, this.takeCoin, null, this);
    this.mines.forEachAlive(function(child){
      if(Math.abs(playerX - child.body.x) <= 16 && Math.abs(playerY - child.body.y) <= 20){
        child.kill();
        playState.playerDie();
      }
    });
  },

  takeSaw: function(){
    var playerX = this.player.body.x;
    var playerY = this.player.body.y;
    //game.physics.ninja.overlap(this.player, this.coins, this.takeCoin, null, this);
    this.saws.forEachAlive(function(child){
      if(Math.abs(playerX - child.body.x) <= 16 && Math.abs(playerY - child.body.y) <= 20 && playState.player.alive){
        playState.playerDie();
      }
    });
  },

  takeKey: function(){
    if(Math.abs(this.player.body.x - this.key.x - 5) <= 16 && Math.abs(this.player.body.y - this.key.y - 5) <= 20 && this.key.alive){
      this.key.kill();
      this.getKey = true;
      this.gate.animations.play('open');
    }
  },

  clear: function(){
    if(Math.abs(this.player.body.x - this.gate.x - 10) <= 21 && Math.abs(this.player.body.y - this.gate.y - 15) <= 30 && this.getKey && !this.isClear){
      this.clearSound.play();
      game.global.gameBGM.fadeOut(800);
      game.time.events.add(5100, function(){
        game.global.gameBGM.stop();
        game.global.gameBGM = null;
        game.state.start('menu');
      }, this);
      this.isClear = true;
    }
  },

  //---control prop---//
  addProp: function () {
    console.log(this.haveProp);
    var p;
    var playerX = this.player.body.x;
    var playerY = this.player.body.y;
    if (this.haveProp == 'sworditem') {
      //console.log('true');
      this.prop = game.add.sprite(playerX, playerY, 'sword');
      this.prop.anchor.setTo(0.5, 0.5);
      this.prop.scale.setTo(0.1, 0.1);
      game.physics.ninja.enableAABB(this.prop);
      this.prop.body.gravityScale = 0;
      this.shellsNumber = 5;
    }
    else if (this.haveProp == 'gun') {
      this.prop = game.add.sprite(playerX, playerY, 'gun');
      this.prop.anchor.setTo(0.5, 0.5);
      game.physics.ninja.enableAABB(this.prop);
      this.prop.body.gravityScale = 0;
      this.shellsNumber = 5;
    }
    else if (this.haveProp == 'shotgun') {
      this.prop = game.add.sprite(playerX, playerY, 'shotgun');
      this.prop.anchor.setTo(0.5, 0.5);
      this.prop.scale.setTo(0.12, 0.12);
      game.physics.ninja.enableAABB(this.prop);
      this.prop.body.gravityScale = 0;
      this.shellsNumber = 5;
    }
    else if (this.haveProp == 'pretext') {
      this.prop = game.add.sprite(playerX, playerY, 'pretext');
      this.prop.anchor.setTo(0.5, 0.5);
      this.prop.scale.setTo(0.02, 0.02);
      game.physics.ninja.enableAABB(this.prop);
      this.prop.body.gravityScale = 0;
      this.shellsNumber = 10;
    }
    else if (this.haveProp == 'shield') {
      this.prop = game.add.sprite(playerX, playerY, 'shield');
      this.prop.anchor.setTo(0.5, 0.5);
      this.prop.scale.setTo(0.3, 0.3);
      game.physics.ninja.enableAABB(this.prop);
      this.prop.body.gravityScale = 0;
      this.shellsNumber = 5;
    }
    else {
      if (this.prop.alive == true) {
        this.prop.kill();
        console.log('break');
      }

    }
  },

  changeEQ: function () {
    if (this.keyQ.isUp)
      this.QisDown = false;
      //console.log(this.haveProp);
    if(this.swordSound.isPlaying &&　this.haveProp != 'sworditem'){
      this.swordSound.stop();
    }
      
  },

  controlShell: function () {
    this.shells.forEachAlive(function (shell) {
      if (shell.body.facing == 1) {
        if (shell.type == 0)
          shell.reset(shell.x + 5, shell.y - 6);
        else if (shell.type == 1)
          shell.reset(shell.x + 5, shell.y + 6);
        else if (shell.type == 2)
          shell.reset(shell.x + 5, shell.y + 3);
        else if (shell.type == 3)
          shell.reset(shell.x + 5, shell.y);
        else if (shell.type == 4)
          shell.reset(shell.x + 5, shell.y - 3);
      }
      else if (shell.body.facing == -1) {
        if (shell.type == 0)
          shell.reset(shell.x - 5, shell.y - 6);
        else if (shell.type == 1)
          shell.reset(shell.x - 5, shell.y + 6);
        else if (shell.type == 2)
          shell.reset(shell.x - 5, shell.y + 3);
        else if (shell.type == 3)
          shell.reset(shell.x - 5, shell.y);
        else if (shell.type == 4)
          shell.reset(shell.x - 5, shell.y - 3);
      }
    })
  },

  detectHaveProp: function () {// detect player pick up the prop, prop is an array
    //for (var i = 0; i < prop.length; i++) {
    game.physics.ninja.overlap(this.player, this.proparr, this.chooseProp, null, this);
    //}
  },

  chooseProp: function (player, prop) {
    console.log("overlap");
    prop.kill();
    if (this.haveProp != '') {
      this.prop.kill();
    }
    this.haveProp = prop.key;
    this.addProp();
  },

  controlProp: function () {
    if (this.prop != null && this.prop.alive == true) {
      if (this.haveProp == 'shield') {
        this.prop.reset(this.player.x, this.player.y);
      }
      else {
        if (this.player.scale.x > 0) {
          this.prop.reset(this.player.x + 20, this.player.y);
          if (this.prop.scale.x > 0)
            this.prop.scale.x *= -1;
        }

        else {
          this.prop.reset(this.player.x - 20, this.player.y);
          if (this.prop.scale.x < 0)
            this.prop.scale.x *= -1;
        }
      }


    }
  },

  propSwordFunction: function (enemies) {//enemies is an array or group
    if (this.haveProp == 'sworditem') {
      this.keyQ.onDown.add(function() {if(this.haveProp == 'sworditem'){this.swordSound.play(); this.swordSound.loop = true;}}, this);
      this.keyQ.onUp.add(function () {if(this.haveProp == 'sworditem'){this.swordSound.stop();}}, this);
      if(this.keyQ.isDown){
        if(this.prop.angle == 0){
          if(this.prop.scale.x < 0){
            game.add.tween(this.prop).to({angle: 90}, 100).yoyo(true).start();
          }
          else{
            game.add.tween(this.prop).to({angle: -90}, 100).yoyo(true).start();
          }
        }
        game.physics.ninja.overlap(this.prop, enemies, function (prop, enemy) { console.log('kill'); this.shellsNumber--; enemy.kill(); game.global.score += 5; this.scoreLabel.text = 'score:' + game.global.score;}, null, this);
        if (this.shellsNumber == 0) {
          //console.log('break');
          this.haveProp = '';
          this.addProp(this.haveProp);
        }
      }
    }
  },

  propGunFunction: function (enemies) {//enemies is an array or group
    if (this.haveProp == 'gun' && this.shellsNumber > 0) {
      if (this.keyQ.isDown && this.QisDown == false) {
        this.gunSound.play();
        var shell = this.shells.getFirstDead();
        shell.anchor.setTo(0.5, 0.5);
        shell.scale.setTo(0.7, 0.7);
        shell.reset(this.prop.x, this.prop.y);
        game.physics.ninja.enableAABB(shell);
        if (this.prop.scale.x < 0) {
          shell.body.facing = 1;
          shell.scale.x *= -1;
          shell.type = 3;
        }
        else {
          shell.body.facing = -1;
          shell.type = 3;
        }

        this.shellsNumber--;
        this.QisDown = true;
      }
      if (this.shellsNumber == 0) {
        this.haveProp = '';
        this.addProp(this.haveProp);
      }
    }
  },

  propShotgunFunction: function (enemies) {//enemies is an array or group
    if (this.haveProp == 'shotgun' && this.shellsNumber > 0) {
      if (this.keyQ.isDown && this.QisDown == false) {
        this.shotgunSound.play();
        for (var i = 0; i < 5; i++) {
          var shell = this.shells.getFirstDead();
          shell.anchor.setTo(0.5, 0.5);
          shell.scale.setTo(0.7, 0.7);
          shell.reset(this.prop.x, this.prop.y);
          game.physics.ninja.enableAABB(shell);
          if (this.prop.scale.x < 0) {
            shell.body.facing = 1;
            shell.scale.x *= -1;
            shell.type = i;
          }
          else {
            shell.body.facing = -1;
            shell.type = i;
          }
        }


        this.shellsNumber--;
        this.QisDown = true;
      }
      if (this.shellsNumber == 0) {
        this.haveProp = '';
        this.addProp(this.haveProp);
      }
    }
  },

  propPretextFunction: function (bullet) {
    if (this.haveProp == 'pretext') {
      if (this.keyQ.isDown) {
        this.prop.scale.setTo(0.02, 0.05);
        this.QisDown = true;
        game.physics.ninja.overlap(this.prop, bullet, function (prop, bullet) { this.pretextSound.play(); this.shellsNumber--; bullet.kill(); }, null, this);
        if (this.shellsNumber == 0) {
          //console.log('break');
          this.haveProp = '';
          this.addProp(this.haveProp);
        }
      }
      if (!this.QisDown) {
        this.prop.scale.setTo(0.02, 0.02);
      }
    }
  },

  propShieldFunction: function (bullet) {
    if (this.haveProp == 'shield') {
      game.physics.ninja.overlap(this.prop, bullet, function (prop, bullet) { this.shieldSound.play(); this.shellsNumber--; bullet.kill(); }, null, this);
      if (this.shellsNumber == 0) {
        //console.log('break');
        this.haveProp = '';
        this.addProp(this.haveProp);
      }
    }
  },

  detectShellCollide: function (enemies) {//input should be four type: static enemy, dynamic enemy, static enemy bullet, dynamic enemy bullet
    var game = this.game;
    var tiles = this.tiles;
    this.shells.forEachAlive(function (bullet) {
      enemies.forEachAlive(function (enemy) {
        if (Math.abs(enemy.x - bullet.x) <= 10 && Math.abs(enemy.y - bullet.y) <= 10) {
          game.global.score += 5;
          playState.scoreLabel.text = 'score:' + game.global.score;
          bullet.kill();
          enemy.kill();
        }
      })
      //game.physics.ninja.overlap(bullet, enemies, function (bullet, enemy) { bullet.kill(); enemy.kill(); }, null, game);
      for (var i = 0; i < tiles.length; i++) {
        if (Math.abs(tiles[i].tile.x - bullet.x) <= 10 && Math.abs(tiles[i].tile.y - bullet.y) <= 12)
          bullet.kill();
      }
    }, this);
  }
}