function portal(portals, player, portalEnter){
  portals.forEach(function(child){
    if(portalEnter == 0){
      if(Math.abs(player.body.x - child.x - 20) < 31 && Math.abs(player.body.y - child.y - 20) < 35){
        var newIndex = (portals.getIndex(child) + 3) % 7;
        var newPortal = portals.getChildAt(newIndex);
        //console.log(newIndex);
        player.reset(newPortal.x, newPortal.y);

        var angle;
        if(newIndex == 0){
          angle = 6;
        }
        else if(newIndex == 1){
          angle = 4.6;
        }
        else if(newIndex == 2){
          angle = 3.14;
        }
        else if(newIndex == 3){
          angle = 2.1;
        }
        else if(newIndex == 4){
          angle = 1.65;
        }
        else if(newIndex == 5){
          angle = 0.6;
        }
        else{
          angle = 6.2;
        }

        player.body.moveDown(300 * Math.sin(angle));
        player.body.moveRight(300 * Math.cos(angle));

        portalEnter = 1;
      }
    }
    else{
      var smallFlag = false;
      portals.forEach(function(child){
        if(Math.abs(player.body.x - child.x - 20) <= 31 && Math.abs(player.body.y - child.y - 20) <= 35){
          smallFlag = true;
        }
      })
      
      if(smallFlag == false){
        portalEnter = 0;
      }
    }
  });

  return portalEnter;
}