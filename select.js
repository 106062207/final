var selectState = {
  create: function() {
    var cntX = game.width/2;
    var cntY = game.height/2;
    var font = {font:'bold 25px Arial', fill:'#ffffff'};
    var background = game.add.sprite(cntX, cntY - 100, 'menu');
    background.anchor.setTo(0.5, 0.5);

    this.rains = initGroup();
    this.rains.createMultiple(120, 'rain');
    this.rains.setAll('body.gravityScale', 0.6);

    this.first = game.add.text(150, 100, '1', { font: '72px Arial', fill: '#fff' });  
    this.second = game.add.text(450, 100, '2', { font: '72px Arial', fill: '#fff' });
    this.third = game.add.text(750, 100, '3', { font: '72px Arial', fill: '#fff' });
    this.fourth = game.add.text(300, 400, '4', { font: '72px Arial', fill: '#fff' });
    this.fifth = game.add.text(600, 400, '5', { font: '72px Arial', fill: '#fff' });

    this.first.inputEnabled = true;
    this.second.inputEnabled = true;
    this.third.inputEnabled = true;
    this.fourth.inputEnabled = true;
    this.fifth.inputEnabled = true;

    this.select1 = game.add.image(80, 200, 'select1');
    this.select1.scale.setTo(0.2, 0.2);
    this.select2 = game.add.image(380, 200, 'select2');
    this.select2.scale.setTo(0.2, 0.2);
    this.select3 = game.add.image(680, 200, 'select3');
    this.select3.scale.setTo(0.2, 0.2);
    this.select4 = game.add.image(230, 500, 'select4');
    this.select4.scale.setTo(0.2, 0.2);
    this.select5 = game.add.image(530, 500, 'select5');
    this.select5.scale.setTo(0.2, 0.2);

    var cntX = game.width / 2;
    var font = { font: 'bold 25px Arial', fill: '#ffffff' };

    this.menuText = setText(cntX, game.height * 9 / 10, 'Menu', font, this.inputTexts);
    this.menuText.inputEnabled = true;
  },
  update: function() {
    if(!game.global.titleBGM.isPlaying){
      game.global.titleBGM.play();
    }
      this.startGame();
      raining(this.rains, 30, 920, 50, 550);

    this.menu();
  },
  startGame: function () {
      if (this.first.input.pointerDown()){
        game.global.score = 0;
        game.global.level = 1;
        game.global.titleBGM.fadeOut(1500);

        game.time.events.add(1600, function(){
            game.global.titleBGM = null;
            game.state.start('play');
        });
      }
      else if (this.second.input.pointerDown()){
        game.global.score = 0;
        game.global.level = 2;
        game.global.titleBGM.fadeOut(1500);

        game.time.events.add(1600, function(){
            game.global.titleBGM = null;
            game.state.start('play');
        });
      }
      else if (this.third.input.pointerDown()){
        game.global.score = 0;
        game.global.level = 3;
        game.global.titleBGM.fadeOut(1500);

        game.time.events.add(1600, function(){
            game.global.titleBGM = null;
            game.state.start('play');
        });
      }
      else if (this.fourth.input.pointerDown()){
        game.global.score = 0;
        game.global.level = 4;
        game.global.titleBGM.fadeOut(1500);
        
        game.time.events.add(1600, function(){
            game.global.titleBGM = null;
            game.state.start('play');
        });
      }
      else if (this.fifth.input.pointerDown()){
        game.global.score = 0;
        game.global.level = 5;
        game.global.titleBGM.fadeOut(1500);

        game.time.events.add(1600, function(){
            game.global.titleBGM = null;
            game.state.start('play');
        });
      }
      else {
          if (this.first.input.pointerOver()) {
              this.first.addColor('#ff0000', 0);
          }
          else if (this.second.input.pointerOver()) {
              this.second.addColor('#ff0000', 0);
          }
          else if (this.third.input.pointerOver()) {
              this.third.addColor('#ff0000', 0);
          }
          else if (this.fourth.input.pointerOver()) {
              this.fourth.addColor('#ff0000', 0);
          }
          else if (this.fifth.input.pointerOver()) {
              this.fifth.addColor('#ff0000', 0);
          }
          else {
              this.first.addColor('#ffffff', 0);
              this.second.addColor('#ffffff', 0);
              this.third.addColor('#ffffff', 0);
              this.fourth.addColor('#ffffff', 0);
              this.fifth.addColor('#ffffff', 0);
          }
      }
  },

  menu: function () {
    if(this.menuText.input.pointerOver()){
      this.menuText.addColor('#ff0000', 0);
    }
    else{
      this.menuText.addColor('#ffffff', 0);
    }
    if(this.menuText.input.pointerDown()){
      game.state.start('menu');
    }
  },
}