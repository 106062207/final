var settingState = {
  preload: function(){
      game.stage.backgroundColor = '#000000';
      this.inputTexts = setInputTexts();
  },
  create: function(){
      var cntX = game.width/2;
      var font = {font:'bold 25px Arial', fill:'#ffffff'};

      var musicText = setText(cntX,250,'music',font);
      this.musicbar = game.add.sprite(100,300,'bar');
      var soundText = setText(cntX,450,'sound',font);
      this.soundbar = game.add.sprite(100,500,'bar');
      this.musiccontrol = game.add.sprite(0,0,'whiteCircle');
      this.musiccontrol.reset(this.musicbar.x+this.musicbar.width*game.global.musicvolume-this.musiccontrol.width/2,this.musicbar.y+this.musicbar.height/2-this.musiccontrol.height/2);
      this.soundcontrol = game.add.sprite(0,0,'whiteCircle');
      this.soundcontrol.reset(this.soundbar.x+this.soundbar.width*game.global.soundvolume-this.soundcontrol.width/2,this.soundbar.y+this.soundbar.height/2-this.soundcontrol.height/2);
      var MenuText = setText(cntX, game.height*9/10, 'Menu', font, this.inputTexts);

      MenuText.events.onInputDown.add(this.menu, this);

      this.musiccontrol.inputEnabled = true;
      this.soundcontrol.inputEnabled = true;
      
      this.musiccontrol.draggable = true;
      this.musiccontrol.input.enableDrag(true);
      this.musiccontrol.input.allowVerticalDrag = false;
      this.soundcontrol.draggable = true;
      this.soundcontrol.input.enableDrag(true);
      this.soundcontrol.input.allowVerticalDrag = false;        
  },
  update: function(){
      this.changemusicvol();
      this.changesoundvol();
  },
  menu: function(){
      game.state.start('menu');
  },
  changemusicvol: function(){
      if(this.musiccontrol.x+this.musiccontrol.width>this.musicbar.x+this.musicbar.width){
          this.musiccontrol.reset(this.musicbar.x+this.musicbar.width-this.musiccontrol.width/2,this.musicbar.y+this.musicbar.height/2-this.musiccontrol.height/2);
      }
      else if(this.musiccontrol.x+this.musiccontrol.width<this.musicbar.x){
          this.musiccontrol.reset(this.musicbar.x-this.musiccontrol.width/2,this.musicbar.y+this.musicbar.height/2-this.musiccontrol.height/2);
      }
      else{
          this.musiccontrol.reset(this.musiccontrol.x,this.musicbar.y+this.musicbar.height/2-this.musiccontrol.height/2);
      }
      game.global.musicvolume = (this.musiccontrol.x+this.musiccontrol.width/2-this.musicbar.x)/this.musicbar.width;
      game.global.titleBGM.volume = game.global.musicvolume;
  },
  changesoundvol: function(){
      if(this.soundcontrol.x+this.soundcontrol.width>this.soundbar.x+this.soundbar.width){
          this.soundcontrol.reset(this.soundbar.x+this.soundbar.width-this.soundcontrol.width/2,this.soundbar.y+this.soundbar.height/2-this.soundcontrol.height/2);
      }
      else if(this.soundcontrol.x+this.soundcontrol.width<this.soundbar.x){
          this.soundcontrol.reset(this.soundbar.x-this.soundcontrol.width/2,this.soundbar.y+this.soundbar.height/2-this.soundcontrol.height/2);
      }
      else{
          this.soundcontrol.reset(this.soundcontrol.x,this.soundbar.y+this.soundbar.height/2-this.soundcontrol.height/2);
      }
      game.global.soundvolume = (this.soundcontrol.x+this.soundcontrol.width/2-this.soundbar.x)/this.soundbar.width;
  },
}