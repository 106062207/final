function setInputTexts() {
  var inputTexts = game.add.group();

  inputTexts.inputEnableChildren = true;
  inputTexts.onChildInputOver.add(hoverOver, this);
  inputTexts.onChildInputOut.add(hoverOut, this);

  return inputTexts;
}

function setText(posX, posY, text, font, group) {
  var item = game.add.text(posX, posY, text, font);

  item.anchor.setTo(0.5, 0.5);
  if (group != null) group.add(item);

  return item;
}

function dialogue(state, index) {
  var delay = state.delays[index];
  var messages = state.messages;
  state.text.text = messages[index];
  if (index < messages.length - 1) {
    if (delay >= 0) {
      game.time.events.add(delay * 100, dialogue, this, state, index+1);
    } else {
      state.exception(state, index);
    }
  } else if (index == messages.length - 1) {
    game.time.events.add(delay * 100, function() {game.state.start('menu')}, this);
  }
}

function hoverOver(item) {
  item.fill = '#ff0000';
}

function hoverOut(item) {
  item.fill = '#ffffff';
}
