var tutorialState = {
  preload: function() {
    game.stage.backgroundColor = '#CCCCCC';
    this.walls = initGroup();
    this.tiles = initGroup();
    this.traps = initGroup();
    this.bullets = initGroup();
    this.drops = game.add.group();
    this.text = setText(game.width/2, 100, "...", {font:'bold 50px Arial', fill:'#ffffff'});
    cursors = game.input.keyboard.createCursorKeys();

    this.keys = setupKeys();
  },
  create: function() {
    this.player = setupPlayer(game.width/2, game.height/2, 'playerSheet');
    this.player.inBag = '';
    this.player.ammos = this.bullets;
    addSprite(game.width/2 - 250, game.height - 250, 'steelBone', 1, this.walls);
    addSprite(game.width/2 - 125, game.height - 250, 'steelBone', 1, this.walls);
    addSprite(game.width/2, game.height - 250, 'steelBone', 1, this.walls);
    addSprite(game.width/2 + 125, game.height - 250, 'steelBone', 1, this.walls);
    addSprite(game.width/2 + 250, game.height - 250, 'steelBone', 1, this.walls);
    addTile(60, game.height - 60, 2, 'steelSlope', 3, this.tiles, 1, 1);
    addTile(game.width - 60, game.height - 60, 3, 'steelSlope', 3, this.tiles, -1, 1);
    setWallsPhysics(this.walls);
    
    game.physics.ninja.enableAABB([this.walls, this.player]);
    // Tutorial messages
    game.time.events.add(1000, dialogue, this, this, 0);
  },
  update: function() {
    game.physics.ninja.collide(this.player, this.walls);
    game.physics.ninja.collide(this.player, this.tiles);
    moveByCursors2(this.player, cursors, this.tiles);

    this.keys.Q.onDown.add(attack, this, 0, this.player);

    game.physics.ninja.overlap(this.player, this.drops, getDrop, null, this);
    game.physics.ninja.overlap(this.player, this.traps, hitPlayer, null, this);

    moveTraps(this.traps);
    moveBullets(this.bullets);
    hitTrap(this.bullets, this.traps);

    holding(this.player);
    wearOut(this.player);
  },
  delays: [15, 15, 10, -1, 15,
           10, -1, -1, 10, -1,
           -1, 15, 10, 10],
  messages: ["Welcome rookie...",
  "You'll learn some basic skills here...",
  "Get prepared!!",
  "Press cursors to move\nLeft, Up, Right",
  "Hmm...\nYou're a quick learner aren't you...",

  "Good, let us continue...",
  "Now, get yourself a sword...",
  "Destroy the mines!!\npress Q to slash",
  "Nice, now let's do some shooting...",
  "pick up the shotgun",

  "Clear all the saws!!\nBut watch out, they're sharp...",
  "This is the end of tutorial...",
  "Good Luck...",
  "You'll need it..."],
  exception: function(state, index) {
    var midX = game.width/2;
    var midY = game.height/2;
    switch(index) {
      case 3:
        this.movePractice(state, index);
        break;
      case 6:
        var sword = addWeapon(midX, midY + 50, 'sword', this.drops);
        sword.events.onKilled.add(function() {game.time.events.add(0, dialogue, this, state, index+1)});
        break;
      case 7:
        this.swordPractice(state, index);
        break;
      case 9:
        var shotgun = addWeapon(midX, midY + 350, 'shotgun', this.drops);
        shotgun.events.onKilled.add(function() {game.time.events.add(0, dialogue, this, state, index+1)});
        break;
      case 10:
        this.gunPractice(state, index);
        break;
    }
  },
  movePractice: function(state, index) {
    var lefts = game.add.group();
    var ups = game.add.group();
    var rights = game.add.group();
    var offX = game.width/2 - 75*4;
    var types = ['up', 'left', 'right', 'up', 'left', 'up', 'right', 'left', 'right'];

    for (var i = 0; i < 9; i++) {
      switch(types[i]) {
        case 'left':
          addSprite(offX + 75*i, 200, 'cursorRight', 1, lefts, -1, 1);
          break;
        case 'up':
          addSprite(offX + 75*i, 200, 'cursorDown', 1, ups, 1, -1);
          break;
        case 'right':
          addSprite(offX + 75*i, 200, 'cursorRight', 1, rights, 1, 1);
          break;
      }
    }
    cursors.left.onDown.addOnce(function() {
      lefts.killAll();
      if (isFinish(lefts, ups, rights)) {
        game.time.events.add(0, dialogue, this, state, index+1);
      }
    }, this);
    cursors.up.onDown.addOnce(function() {
      ups.killAll();
      if (isFinish(lefts, ups, rights)) {
        game.time.events.add(0, dialogue, this, state, index+1);
      }
    }, this);
    cursors.right.onDown.addOnce(function() {
      rights.killAll();
      if (isFinish(lefts, ups, rights)) {
        game.time.events.add(0, dialogue, this, state, index+1);
      }
    }, this);
  },
  swordPractice: function(state, index) {
    var midX = game.width/2;
    var midY = game.height/2;
    for (var i = 1; i < 3; i++) {
      addTrap(midX - 50*i, midY + 50*i, 'mine', this.traps);
      addTrap(midX + 50*i, midY + 50*i, 'mine', this.traps);
    }
    addTrap(midX, midY + 50, 'mine', this.traps);
    this.traps.forEachAlive(function(trap) {
      trap.events.onKilled.add(function() {
        if (isFinish(this.traps)) game.time.events.add(0, dialogue, this, state, index+1);
      }, this, 0);
    }, this);
  },
  gunPractice: function(state, index) {
    addTrap(50, game.height - 140, 'saw', this.traps, [120, 610, 120, 850], [1, 1, 1, -1], [1, 0, -1, 0]);
    this.traps.forEachAlive(function(trap) {
      trap.events.onKilled.add(function() {
        if (isFinish(this.traps)) game.time.events.add(0, dialogue, this, state, index+1);
      }, this, 0);
    }, this);
  }
}